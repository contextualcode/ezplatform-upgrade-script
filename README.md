# eZ Platform Upgrade Script

The repo provides a script which makes eZ Platform upgrades a bit more simpler.

## Installation

```bash
wget https://gitlab.com/contextualcode/ezplatform-upgrade-script/-/raw/master/bin/ezplatform-upgrade.sh -O /usr/local/bin/ezplatform-upgrade && chmod +x /usr/local/bin/ezplatform-upgrade
```

## Usage

1. Clone your project. If you are trying to upgrade a running installation this step should be skipped:
    ```bash
    cd ~/Projects
    git clone <repo-url> <project-name>
    cd <project-name> 
    ``` 

2. Run the installed `ezplatform-upgrade` script. Please note it expects two arguments:
    - eZ Platform version, [`3.0.0`](https://github.com/ezsystems/ezplatform/releases/tag/v3.0.0) is a default value
    - Git [recursive merge strategy](https://git-scm.com/docs/merge-strategies#Documentation/merge-strategies.txt) option, [`patience`](https://git-scm.com/docs/merge-strategies#Documentation/merge-strategies.txt-patience) is a default value
    ```bash
    ezplatform-upgrade 3.0.0 theirs
    ```

3. Resolve merge conflicts and commit/push your new `upgrade-3.0.0` branch.

4. After QA and tests at `upgrade-3.0.0` branch it could be deployed on production:
    ```bash
    git checkout master
    git merge upgrade-3.0.0
    git push origin master
    ```